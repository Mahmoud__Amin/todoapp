 import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubit/cubit.dart';
import 'cubit/states.dart';

class CounterScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context)=>CounterCubit(),
      child: BlocConsumer<CounterCubit,CounterStates>(
        listener:(BuildContext context,CounterStates state){
        },
        builder:(BuildContext context,CounterStates state)=>Scaffold(
          appBar:AppBar(
            title: const Text("Counter"),
          ),
          body: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {
                    CounterCubit.get(context).minus();
                  },
                  child: const Text("Minus",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold)),),
                Text('${CounterCubit.get(context).counter}',style:TextStyle(fontSize: 35,fontWeight: FontWeight.bold),),
                TextButton(
                  onPressed: () {
                    CounterCubit.get(context).plus();
                  },
                  child: const Text("Plus",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold)),),

              ],
            ),
          ),
        ),


      ),
    );
  }
}
