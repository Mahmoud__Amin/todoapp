
import 'dart:ui';

import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:to_do_app/shared/cubit/cubit.dart';

Widget buildTextItem(tasks, context) {
  return Dismissible(
    key: Key(tasks['id'].toString()),
    onDismissed: (direction) {
      var CurrentTask=tasks;
      AppCubit.get(context).deleteFromDataBase(id: tasks['id']);
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
          backgroundColor: Colors.blue,
          content: Row(mainAxisSize: MainAxisSize.max, children: [
            const Expanded(
                child: Text(
                  'Task Deleted!',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                )),
            const Expanded(
              child: SizedBox(
                width: 120,
              ),
            ),
            Expanded(
              child: IconButton(
                icon: const Text('Undo',
                    style:
                    TextStyle(fontSize: 15, fontWeight: FontWeight.w500)),
                onPressed: () {
                  AppCubit.get(context).insertAfterDelete(
                      title: CurrentTask['title'],
                      time: CurrentTask['time'],
                      date: CurrentTask['date'],
                      status: CurrentTask['status']);
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                },
              ),
            ),
          ])));
    },
    child: Padding(
      padding: const EdgeInsets.all(20),
      child: Row(
        children: [
          CircleAvatar(
            radius: 40.0,
            child: Text(
              tasks['time'],
            ),
          ),
          const SizedBox(
            width: 20.0,
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(tasks['title'],
                    style: const TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.bold)),
                Text(tasks['date'], style: const TextStyle(color: Colors.grey)),
              ],
            ),
          ),
          const SizedBox(
            width: 20.0,
          ),
          IconButton(
            icon: const Icon(Icons.check_box),
            color: Colors.green,
            onPressed: () {
              AppCubit.get(context).updateData(status: 'done', id: tasks['id']);
            },
          ),
          IconButton(
            icon: const Icon(Icons.archive_outlined),
            color: Colors.black26,
            onPressed: () {
              AppCubit.get(context)
                  .updateData(status: 'archived', id: tasks['id']);
            },
          )
        ],
      ),
    ),
  );
}

Widget buildTaskItem(var tasks, context) {
  return ConditionalBuilder(
    builder: (BuildContext ctx) {
      return ListView.separated(
          itemBuilder: (BuildContext context, int index) {
            return buildTextItem(tasks[index], ctx);
          },
          separatorBuilder: (BuildContext context, int index) => Padding(
                padding: const EdgeInsetsDirectional.only(
                  start: 20.0,
                ),
                child: Container(
                  width: double.infinity,
                  height: 1.0,
                  color: Colors.grey[300],
                ),
              ),
          itemCount: tasks.length);
    },
    condition: (tasks.isNotEmpty),
    fallback: (BuildContext context) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(
              Icons.menu,
              size: 150.0,
              color: Colors.grey,
            ),
            Text(
              "No Tasks Yet Please Add Some Tasks ",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey),
            )
          ],
        ),
      );
    },
  );
}
