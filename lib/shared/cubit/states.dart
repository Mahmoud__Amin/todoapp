abstract class AppStates {}

class AppInitialState extends AppStates {}

class AppChangeBottomNavigationBarState extends AppStates {}

class AppChangeBottomSheetState extends AppStates {}

class AppCreateDatabaseState extends AppStates {}

class AppInsertIntoDatabaseState extends AppStates {}

class AppGetDatabaseState extends AppStates {}

class AppUpdateDatabaseState extends AppStates {}

class AppGetDatabaseLoadingState extends AppStates {}

class AppDeleteDatabaseState extends AppStates {}
