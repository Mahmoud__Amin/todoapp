import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';
import 'package:to_do_app/modules/archived_tasks/archived_tasks_screen.dart';
import 'package:to_do_app/modules/done_tasks/done_tasks_screen.dart';
import 'package:to_do_app/modules/new_tasks/new_tasks_screen.dart';
import 'states.dart';

class AppCubit extends Cubit<AppStates> {
  AppCubit() : super(AppInitialState());
  static AppCubit get(context) => BlocProvider.of(context);
  int currentIndex = 0;
  List<Widget> screens = [
    NewTasksScreen(),
    DoneTasksScreen(),
    ArchivedTasksScreen(),
  ];
  List<String> titles = ["Tasks", "Done Tasks", "Archived Tasks"];

  void ChangeIndex(Index) {
    currentIndex = Index;
    emit(AppChangeBottomNavigationBarState());
  }


  late Database database;
  List<Map> newtasks = [];
  List<Map> donetasks = [];
  List<Map> archivedtasks = [];

  Future<void> createDataBase() async {
    openDatabase('todo.db', version: 1, onCreate: (database, version) {
      print('database created');
      database
          .execute(
              'CREATE TABLE tasks (id INTEGER PRIMARY KEY,title TEXT, date TEXT, time TEXT,status TEXT)')
          .then((value) {
        print('Table Created');
      }).catchError((error) {
        print('Error When Creating Table ${error.toString()}');
      });
    }, onOpen: (database) {
      getDataFromDataBase(database);
      print(('database opened'));
    }).then((value) {
      database = value;
      emit(AppCreateDatabaseState());
    });
  }

  void insertToDataBase(
      {
      required String title,
      required String time,
      required String date}) async {
      await database.transaction((txn) => txn
            .rawInsert(
                "insert into tasks(title,time,date,status) values('$title','$time','$date','new')")
            .then((value) {
          emit(AppInsertIntoDatabaseState());
          getDataFromDataBase(database);
          print("$value inserted successfully");
        }).catchError((error) {
          print("Error When Inserting New Record ${error.toString()}");
        }));
  }

  void getDataFromDataBase(database) {
    newtasks = [];
    donetasks = [];
    archivedtasks = [];
    emit(AppGetDatabaseLoadingState());
    database.rawQuery('SELECT * from tasks').then((value) {
      value.forEach((element) {
        if (element['status'] == 'new') {
          newtasks.add(element);
        } else if (element['status'] == 'done') {
          donetasks.add(element);
        } else {
          archivedtasks.add(element);
        }
      });
      emit(AppGetDatabaseState());
    });
  }
  void updateData({required String status, required int id}) async {
    database.rawUpdate('UPDATE tasks SET status = ? WHERE id = ?',
        ['$status', id]).then((value) {
      getDataFromDataBase(database);
      emit(AppUpdateDatabaseState());
    });
  }

  void deleteFromDataBase({required id}) {
    database.rawDelete('DELETE FROM tasks WHERE id = ?', [id]).then((value) {
      getDataFromDataBase(database);
      emit(AppDeleteDatabaseState());
    });
  }

    void insertAfterDelete(
      {required String title, required String  time, required String date, required String status}) {
      database.rawInsert(
              "insert into tasks(title,time,date,status) values('$title','$time','$date','$status')")
              .then((value) {
            //emit(AppInsertIntoDatabaseState());
            getDataFromDataBase(database);
          });
    }
    bool isButtomSheetShown = false;
    IconData fabIcon = Icons.edit;

    void changeBottomSheetState(
        {required bool isShow, required IconData icon}) {
      isButtomSheetShown = isShow;
      fabIcon = icon;
      emit(AppChangeBottomSheetState());
    }
  }

