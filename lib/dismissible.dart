import 'package:flutter/material.dart';

class MyApp5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: dismissible(),
    );
  }
}

class dismissible extends StatefulWidget {
  @override
  _dismissibleState createState() => _dismissibleState();
}

class _dismissibleState extends State<dismissible> {
  final li = List<String>.generate(20, (index) => "Item Num ${index + 1}");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
          itemCount: li.length,
          itemBuilder: (ctx, index) {
            final item = li[index];
            return Dismissible(
              key: Key(item),
              onDismissed: (DismissDirection dir) {
                setState(() {
                  li.removeAt(index);
                });
                ScaffoldMessenger.of(ctx).showSnackBar(SnackBar(
                  content: Text(dir == DismissDirection.startToEnd
                      ? "$item Deleted"
                      : "$item Liked"),
                  action: SnackBarAction(
                    label: 'Undo',
                    onPressed: () {
                      setState(() {
                        li.insert(index, item);
                      });
                    },
                  ),
                ));
              },
              confirmDismiss: (DismissDirection dir) async {
                if (dir == DismissDirection.startToEnd) {
                  final bool res = await showDialog(
                      context: context,
                      builder: (BuildContext ctx) {
                        return AlertDialog(
                          content:
                              Text("Are You Sure You Want To Delete $item ?"),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(ctx).pop();
                                },
                                child: Text("Cancel"),
                                style: TextButton.styleFrom(
                                  primary: Colors.black,
                                )),
                            TextButton(
                                onPressed: () {
                                  setState(() {
                                    li.removeAt(index);
                                  });
                                  Navigator.of(ctx).pop();
                                },
                                child: Text("Delete"),
                                style: TextButton.styleFrom(
                                  primary: Colors.red,
                                )),
                          ],
                        );
                      });
                  return res;
                }
                else{
                  return true;
                }
              },
              child: ListTile(
                title: Center(
                  child: Text(item),
                ),
              ),
              background: Container(
                color: Colors.red,
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                    Text(
                      "Delete",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    )
                  ],
                ),
              ),
              secondaryBackground: Container(
                color: Colors.green,
                alignment: Alignment.centerRight,
                child: Icon(Icons.thumb_up),
              ),
            );
          }),
    );
  }
}
