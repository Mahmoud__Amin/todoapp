import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:to_do_app/shared/cubit/cubit.dart';
import 'package:to_do_app/shared/cubit/states.dart';

class HomeLayout extends StatelessWidget {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  var formKey = GlobalKey<FormState>();
  var titleController = TextEditingController();
  var timeController = TextEditingController();
  var dateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => AppCubit()..createDataBase(),
      child: BlocConsumer<AppCubit, AppStates>(
          listener: (BuildContext context, AppStates state) {
        if (state is AppInsertIntoDatabaseState) {
            Navigator.pop(context);
        }
      }, builder: (BuildContext context, AppStates state) {
        AppCubit cubit = AppCubit.get(context);
        return Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            title: Text(cubit.titles[cubit.currentIndex]),
          ),
          body: ConditionalBuilder(
            builder: (BuildContext context) =>
                const Center(child: CircularProgressIndicator()),
            condition: state is AppGetDatabaseLoadingState,
            fallback: (BuildContext context) =>
                cubit.screens[cubit.currentIndex],
          ),
          floatingActionButton: FloatingActionButton(
                  onPressed: () {
                  if (cubit.isButtomSheetShown) {
                  if (formKey.currentState!.validate()) {
                  cubit.insertToDataBase(
                      title: titleController.text,
                      time: timeController.text,
                      date: dateController.text);
                  titleController.text = "";
                  timeController.text = "";
                  dateController.text = "";
                }
              } else {
                scaffoldKey.currentState
                    ?.showBottomSheet(
                        (context) => Container(
                              color: Colors.white,
                              padding: const EdgeInsets.all(20.0),
                              child: Form(
                                key: formKey,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    TextFormField(
                                      controller: titleController,
                                      keyboardType: TextInputType.text,
                                      decoration: const InputDecoration(
                                        label: Text('Task Title'),
                                        prefix: Icon(Icons.title),
                                      ),
                                      validator: (String? value) {
                                        if (value!.isEmpty) {
                                          return 'title must not be empty';
                                        }
                                        return null;
                                      },
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    TextFormField(
                                      controller: timeController,
                                      keyboardType: TextInputType.datetime,
                                      onTap: () {
                                        showTimePicker(
                                                context: context,
                                                initialTime: TimeOfDay.now())
                                            .then((value) {
                                          timeController.text =
                                              value!.format(context).toString();
                                          // print(value?.format(context));
                                        });
                                      },
                                      decoration: const InputDecoration(
                                        label: Text('Task Time'),
                                        prefix:
                                            Icon(Icons.watch_later_outlined),
                                      ),
                                      validator: (String? value) {
                                        if (value!.isEmpty) {
                                          return 'time must not be empty';
                                        }
                                        return null;
                                      },
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    TextFormField(
                                      controller: dateController,
                                      keyboardType: TextInputType.datetime,
                                      onTap: () {
                                        showDatePicker(
                                                context: context,
                                                initialDate: DateTime.now(),
                                                lastDate: DateTime.parse(
                                                    '2026-05-03'),
                                                firstDate: DateTime.now())
                                            .then((value) {
                                          dateController.text =
                                              (DateFormat.yMMMd()
                                                  .format(value!));
                                        });
                                      },
                                      decoration: const InputDecoration(
                                        label: Text('Task Date'),
                                        prefix: Icon(Icons.calendar_today),
                                      ),
                                      validator: (value) {
                                        if (value!.isEmpty) {
                                          return 'date must not be empty';
                                        }
                                        return null;
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                        elevation: 20)
                    .closed
                    .then((value) {
                  cubit.changeBottomSheetState(isShow: false, icon: Icons.edit);
                });
                cubit.changeBottomSheetState(isShow: true, icon: Icons.add);
              }
            },
            child: Icon(cubit.fabIcon),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: cubit.currentIndex,
            onTap: (index) {
              cubit.ChangeIndex(index);
            },
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.menu), label: 'Tasks'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.check_circle_outline), label: 'Done'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.archive_outlined), label: 'Archived'),
            ],
          ),
        );
      }),
    );
  }
}
